#!/bin/sh
if [ ! -d src ]; then
    echo "You should call this script from the top level checkout (e.g., call it as ./build/build.sh)"
    exit 1
fi
[ -e "./C:" ] && echo "Move ./C: out of the way first" && exit 1
# Move C: out of the source dir (it breaks installation of the resulting
# .sis). This directory is created when running MobileGTD using a normal
# python installation and pys60-compat.
[ -e "src/C:" ] && mv "src/C:" "./C:"
ensymble py2sis src --uid=0xA0008CDD --vendor="Martin Mauch" --appname "MobileGTD" --caption="MobileGTD unstable" --caps=ReadUserData+WriteUserData MobileGTD.sis
[ -e  "./C:" ] && mv "./C:" "src/C:"
