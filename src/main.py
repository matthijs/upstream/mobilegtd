# SYMBIAN_UID = 0xA0008CDC
# 0xA0008CDC
# 0x2001A1A0

#from logging import traceS60
#tr=traceS60.trace() 
#tr.go()

def run():
    logger = None
    try:
        import sys
        import e32
        import os
        if e32.in_emulator():
            sys.path.append('c:/python/')
            
            
        # Set up logging
        from log.logging import logger as _logger
        logger = _logger
        logger.log_stderr()

       
        # Read config
        from config import config
        import inout
        config.read_configurations()
        inout.io.safe_chdir(config.gtd_directory)
       
        # Yield to the scheduler
        e32.ao_yield()
    
        from model.projects import Projects
        from gui.projects_list.project_list_view import ProjectListView
        from persistence.projects_directory import ProjectsDirectory
    
        directory = os.path.join(config.gtd_directory,'@Projects')

        projects = Projects()
        projects_directory = ProjectsDirectory(projects)
        projects_directory.add_directory(directory)
        projects_directory.add_directory(os.path.join(directory,'@Review'))
        projects_directory.read()
#        projects.process()
        projects_view = ProjectListView(projects)
        projects_view.run()
    except Exception, e:
        import appuifw,traceback,os
        print e
        
        (type, value, tb) = sys.exc_info()
        lines = []

        # Create error message
        for line in traceback.format_exception_only(type, value):
                lines.append(unicode(line))
        lines.append("\n")

        # Create stacktrace
        trace = traceback.extract_tb(tb)
        trace.reverse()

        basedir = os.path.dirname(__file__) + os.sep
        for (file, line, func, code) in trace:
            # Remove the basedir from any filename, since it is not so
            # interesting but takes up precious space.
            if file.startswith(basedir):
                    file = file[len(basedir):]
            lines.append(u'\n%s:%s\nin %s:\n%s\n' % (file, line, func, code))

        # Create and fill an error dialog
        t = appuifw.Text()
        for line in lines:
            if logger:
                logger.log(line,1)
            t.add(line)

        # Put the focus back at the top
        t.set_pos(0)

        # Create a lock to wait on
        lock = e32.Ao_lock()

        # Set up the dialog
        appuifw.app.menu=[(u'Exit', lock.signal)]
    
        appuifw.app.title=u'Error'
        appuifw.app.body=t
        appuifw.app.exit_key_handler=lock.signal

        # Wait until the exit option or button is chosen
        lock.wait()

        # Exit app when script returns
        appuifw.app.set_exit()

    if logger:
        logger.close()

try:
    run()
except:
    import traceback
    traceback.print_exc()

#tr.stop()
