import sys

# sorted() was introduced in 2.4
if sys.version_info[:2] < (2, 4):
    # Define our own version of sorted
    def sorted(iterable, cmp=cmp, key=lambda x: x, reverse = False):
        # Make a list out of the iterable
        lst = list(iterable)
        # Sort the list, using the given comparator and key function
        lst.sort(lambda x, y: cmp(key(x), key(y)))
        # Reverse if requested
        if reverse:
            lst.reverse()
        # Done!
        return lst
else:
    sorted = sorted
